import { CardValue, CardSuit } from "./Cards/CardTypes";
import Card from "./Cards/Card";

export default class Action 
{
    public Value : CardValue
    public Suit : CardSuit
    public ParentName : string
    public isCardBack : boolean

    public SetUp(card : Card)
    {
        this.Value = card.Value;
        this.Suit = card.Suit;
        this.ParentName = card.node.parent.name;
        this.isCardBack = card.node.scaleX ==-1 ? true:false
    }

}
