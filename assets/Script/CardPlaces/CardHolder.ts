import Card from "../Cards/Card";

const {ccclass, property} = cc._decorator;

@ccclass
export default class CardHolder extends cc.Component
 {
     public cards : Card[] = []

     public PushCard(card : Card)
     {
         this.cards.push(card)
     }

     public UnShiftCard(card : Card)
     {
         this.cards.unshift(card)
     }

     public RemoveCard()
     {
         this.cards.pop();
     }
     public Shake()
     {
        let action0 = cc.scaleTo(0.3, 1.15, 1.15)
        let action1 = cc.scaleTo(0.3, 1, 1)
        let action = cc.sequence(action0,action1)
        this.node.runAction(action)
     }
}
