import Card from "../Cards/Card";
import { CardSuit, CardValue, CardState } from "../Cards/CardTypes";
import CardHolder from "./CardHolder";
import GameManager from "../GameManager";

/**
 * A foundation is one of the four piles of cards you aim to fill during the course of the elements.
 * These each consist of a card suit and must contain the cards of that suit in sequential order.
 */
const {ccclass, property} = cc._decorator;

@ccclass 
export default class Foundation extends CardHolder
 {

    public cards: Card[] = [];

    public suit: CardSuit = null;

    public TopCard: Card = null;


    update()
    {
        if(this.TopCard != this.cards[this.cards.length-1])
        {
            this.TopCard = this.cards[this.cards.length-1]
        }
    }

    canAddCard(card: Card): boolean
     {

        if (this.cards.length == 0)
         {
            if(card.Value == CardValue.Ace)
            return true
            else
             return false
        }

        return this.suit == card.Suit && (this.TopCard.Value + 1) == card.Value;
    }

    public AddCard(card : Card)
    {
        if(this.cards.length == 0)
        {
            this.suit = card.Suit
        }
        card.node.parent = null
        this.node.addChild(card.node)
        card.node.position = cc.Vec2.ZERO
        this.cards.push(card)
        card.State = CardState.InFoundation;
        this.TopCard = card
        GameManager.instance.CheckGamewin()
    }

    public RemoveTopCard()
    {
        let card
        if(this.cards.length !=null)
        {
            card = this.cards.pop()
        }
        card.State = CardState.OnCalibrate
        card.node.parent = null
        if(this.cards.length >0)
        {
            this.TopCard = this.cards[this.cards.length-1]
        }
        else
        {
            this.TopCard = null
            this.suit = null
        }
        
    }

}
