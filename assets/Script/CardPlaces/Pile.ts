import Card from "../Cards/Card";
import { CardValue, CardState } from "../Cards/CardTypes";
import Draggable from "../Cards/Draggable";
import CardFlipper from "../Cards/CardFlipper";
import CardHolder from "./CardHolder";
import GameManager from "../GameManager";
import Action from "../Action";

const {ccclass, property} = cc._decorator;

@ccclass 
export default class Pile extends CardHolder
 {
    public TopCard : Card

    public canAddCard(card: Card): boolean
     {

        if (this.cards.length == 0)
         {
             if(card.Value == CardValue.King)
            return true
            else
            return false
        }
        return this.TopCard.Color() !== card.Color() && (this.TopCard.Value - 1) == card.Value;
    }


    update()
    {
        if(this.TopCard != this.cards[this.cards.length-1])
        {
            this.TopCard = this.cards[this.cards.length-1]
        }
    }

    public AddCardObject(card : Card)
    {
        card.State  = CardState.InPile
        card.node.parent = null
        this.node.addChild(card.node)
        this.node.sortAllChildren()
        this.cards.push(card)
        this.TopCard = card
    }

    public AddStackCards(card : Card[])
    {
        while(card.length >0)
        {
            this.AddCardObject(card.shift())
        }
    }

    public RemoveTopCard()
    {
        let card = this.cards.pop()
        if(this.cards.length > 0)
        {
           this.TopCard = this.cards[this.cards.length-1]
        }
        else this.TopCard == null
        card.State = CardState.OnCalibrate
        card.node.parent = null
        if(this.cards.length>0)
        {
            if(this.TopCard.node.scaleX == -1)
            {
                let number = GameManager.instance.ListNumberToAction.pop()
                number = number +1
                GameManager.instance.ListNumberToAction.push(number)
                let action = new Action()
                action.SetUp(this.TopCard)
                GameManager.instance.ListAction.push(action)
                this.TopCard.getComponent(CardFlipper).Flip()
                this.TopCard.getComponent(Draggable).enabled = true
            }
        }
    }
}
