import Card from "../Cards/Card";
import Draggable from "../Cards/Draggable";
import GameManager from "../GameManager";
import CardHolder from "./CardHolder";


const {ccclass, property} = cc._decorator;

@ccclass
export default class PreWaste extends CardHolder
 {
     public AddCardObject(card : Card)
     {
        card.node.parent= null
        this.node.addChild(card.node) 
        this.cards.unshift(card)
        card.getComponent(Draggable).enabled = false
        card.node.setScale(1,1)
        card.ToggleCardFace(true)
        
        card.node.position = cc.Vec2.ZERO
     }

     public ReleaseCard()
     {
         return this.cards.shift()
     }

     public hasCard() : boolean
     {
         return this.cards.length >0
     }

     public GetAllCards() : Card[]
     {
         let cards = []
         cards = this.cards
         this.cards = []
         return cards
     }
}
