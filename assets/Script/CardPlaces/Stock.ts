import Card from "../Cards/Card";
import { CardState } from "../Cards/CardTypes";
import GameManager from "../GameManager";
import Setting from "../Setting/Setting";
import CardFlipper from "../Cards/CardFlipper";
import CardHolder from "./CardHolder";

/**
 * This is the "reserve" of cards. Any cards not
 */
const {ccclass, property} = cc._decorator;

@ccclass 
export default class Stock extends CardHolder
 {
    handleClick : boolean =true

    start()
    {
        this.node.on(cc.Node.EventType.TOUCH_START,this.onclick,this)
    }

    public hasCard(): boolean 
    {
        return this.cards.length > 0;
    }

    public AddCardObject(card : Card,insertMode : boolean = false)
    {
        card.node.parent = null
        if(!insertMode)
        {
            this.node.addChild(card.node)
            this.PushCard(card)
        }
        else
        {
            this.node.insertChild(card.node,0)
            this.UnShiftCard(card)
        }
        card.node.position = cc.Vec2.ZERO
        card.State = CardState.InStock
        card.ToggleCardFace(false)
        card.node.scaleX = -1
    }

    public Draw() : Card
    {
        let card = this.cards.shift()
        return card
    }


    onclick()
    {
        if(this.handleClick)
        {
            this.handleClick=false
            GameManager.instance.DrawFromStock()
            console.log("ClickDraw")
            this.handleClick=true
        }
        else return
    }
}
