import GameManager from "../GameManager";
import Setting from "../Setting/Setting";
import Card from "../Cards/Card";
import { CardState } from "../Cards/CardTypes";
import Draggable from "../Cards/Draggable";
import CardFlipper from "../Cards/CardFlipper";
import PreWaste from "./PreWaste";
import CardHolder from "./CardHolder";

const {ccclass, property} = cc._decorator;

@ccclass 
export default class Waste extends CardHolder
 {

    @property(cc.Node)
    public PreWasteNode : cc.Node = null

    public TopCard : Card = null

    private PreWaste : PreWaste

    private enableUpdate : boolean

    start()
    {
        this.PreWaste = this.PreWasteNode.getComponent(PreWaste)
    }


    update()
    {
        if(this.enableUpdate)
        {
            if(this.cards.length < Setting.NumberToDraw)
            {
                if(this.PreWaste.hasCard())
                {
                    this.BringUpCardFromPreWaste()
                }
                else
                {
                    this.SetDrag()
                }
            }
            else if(this.cards.length > Setting.NumberToDraw)
            {
                let currentCardHide = this.cards.shift()
                this.PreWaste.AddCardObject(currentCardHide)
            }
            else this.SetDrag()
            if(this.cards.length == 0)
            {
                this.TopCard = null
            }
        }
    }

    public hasCard() : boolean
    {
        return this.cards.length >0
    }
    EnableUpdate()
    {
        this.enableUpdate = true
    }

    DisableUpdate()
    {
        this.enableUpdate = false
    }

    public AddCard(card :Card,insertMode : boolean = false)
    {
        this.DisableUpdate()
        card.getComponent(Draggable).enabled = false
        card.node.parent = null
        if(!insertMode)
        {
            this.node.addChild(card.node)
            this.cards.push(card)
        }
        else
        {
            this.cards.unshift(card)
            this.node.insertChild(card.node,0)
        }
        card.ToggleCardFace(true)
        card.State = CardState.InWaste
        this.EnableUpdate()
        if(card.node.scaleX == -1)
        {
            card.getComponent(CardFlipper).Flip()
        }
    }

    SetDrag()
    {
        for(let i=0;i<this.cards.length;i++)
        {
            if(i == this.cards.length-1)
            {
                this.TopCard = this.cards[i]
                this.TopCard.getComponent(Draggable).enabled = true
            }
            else
            {
                this.cards[i].getComponent(Draggable).enabled = false
            }
        }
    }

    public RemoveTopCard()
    {
        let card = this.cards.pop()
    }
    
    public GiveBackToStock() : Card[]
    {
        this.DisableUpdate()
        while(this.cards.length >0)
        {
            let card = this.cards.shift()
            this.PreWaste.AddCardObject(card)
        }
        return this.PreWaste.GetAllCards()
    }


    public BringUpCardFromPreWaste()
    {
        this.AddCard(this.PreWaste.ReleaseCard(),true)
    }
}