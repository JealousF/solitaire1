import GameManager from "../GameManager";
import { CardValue, CardState, CardColor } from "./CardTypes";
import Draggable from "./Draggable";
import Setting from "../Setting/Setting";

const {ccclass, property} = cc._decorator;

@ccclass 
export default class Card extends cc.Component 
{

    @property(cc.SpriteFrame)
     CardBack : cc.SpriteFrame = null

    @property(cc.SpriteFrame)
     CardFace : cc.SpriteFrame = null

    private DisplayValue : cc.Label[] =[]

    private DisplaySuit : cc.Sprite[] =[]

    public Value : CardValue

    public Suit : number

    public State : CardState = CardState.OnCalibrate

    @property(cc.SpriteFrame)
    HeartSuit : cc.SpriteFrame = null
    @property(cc.SpriteFrame)
    DiamondSuit : cc.SpriteFrame = null
    @property(cc.SpriteFrame)
    CloveSuit : cc.SpriteFrame = null
    @property(cc.SpriteFrame)
    SpadeSuit : cc.SpriteFrame = null

    private Sprite : cc.Sprite

    onLoad ()
    {
        this.Sprite = this.node.getComponent(cc.Sprite)
        this.DisplayValue = this.node.getComponentsInChildren(cc.Label)
        this.DisplaySuit = this.node.getComponentsInChildren(cc.Sprite)
        this.ToggleCardFace(false)
    }

    public ToggleCardFace(bool : boolean)
    {
        if(!bool)
        {
            this.Sprite.spriteFrame = this.CardBack
            this.DisplayValue.forEach(label => label.enabled=false)
            this.DisplaySuit.forEach(sprite => sprite.enabled=false)
            this.Sprite.enabled = true
            this.node.setScale(-1,1)
            this.getComponent(Draggable).enabled = false
        }
        else
        {
            this.DisplayValue.forEach(label => label.enabled=true)
            this.DisplaySuit.forEach(sprite => sprite.enabled=true)
            this.Sprite.spriteFrame = this.CardFace
            /**
             * Set Value Display on Card
             */
            if(this.Value ===1)
            {
                this.DisplayValue.forEach(label =>label.string="A")
            }
            else if(this.Value<= 10)
            {
                this.DisplayValue.forEach(label =>label.string = this.Value.toString())
            }
            else
            {
                if(this.Value === 11)
                {
                    this.DisplayValue.forEach(label =>label.string = "J")
                }
                else if(this.Value === 12)
                {
                    this.DisplayValue.forEach(label =>label.string = "Q")
                }
                else if(this.Value ===13)
                {
                    this.DisplayValue.forEach(label =>label.string = "K")
                }
            }
            /**
             * Set Suit Display on Card
             */
            if(this.Suit===1)
            {
                this.DisplaySuit.forEach(sprite =>sprite.spriteFrame = this.HeartSuit)
            }
            else if(this.Suit ===2)
            {
                this.DisplaySuit.forEach(sprite =>sprite.spriteFrame = this.DiamondSuit)
            }
            else if(this.Suit===3)
            {
                this.DisplaySuit.forEach(sprite =>sprite.spriteFrame = this.CloveSuit)
            }
            else if(this.Suit===4)
            {
                this.DisplaySuit.forEach(sprite =>sprite.spriteFrame = this.SpadeSuit)
            }
            this.Sprite.spriteFrame = this.CardFace
        }
    }

    public Color(): CardColor
     {
        return (this.Suit === 1 || this.Suit === 2) ? CardColor.Red : CardColor.Black
    }

    public isDraggable() : boolean
    {
        return (this.getComponent(Draggable).enabled === true)
    }

    public Shake()
    {
        let action0 = cc.scaleTo(Setting.TimeEffect, 1.15, 1.15)
        let action1 = cc.scaleTo(Setting.TimeEffect, 1, 1)
        let action = cc.sequence(action0,action1)
        this.node.runAction(action)
    }

}
