import Card from "./Card";
import Setting from "../Setting/Setting";


const {ccclass, property} = cc._decorator;

@ccclass
export default class CardFlipper extends cc.Component
 {
     private sprite : cc.Sprite
     private card : Card

    onLoad()
    {
        this.sprite = this.getComponent(cc.Sprite)
        this.card = this.getComponent(Card)
    }

    public Flip()
    {
        if(this.node.scaleX == -1)
        {
            var action = cc.scaleTo(Setting.TimeToFlip,1,1)
            this.node.runAction(action)
            this.scheduleOnce(() => this.card.ToggleCardFace(true),Setting.TimeToFlip/2)
        }
        else
        {
            var action = cc.scaleTo(Setting.TimeToFlip,-1,1)
            this.node.runAction(action)
            this.scheduleOnce(()=>this.card.ToggleCardFace(false),Setting.TimeToFlip/2)
        }
    }
}
