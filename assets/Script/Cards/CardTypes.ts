export enum CardSuit 
{
    Heart =1,
    Diamond =2,
    Spade =3,
    Clove =4
}

export enum CardValue
 {
    Ace = 1,
    Two = 2,
    Three = 3,
    Four = 4,
    Five = 5,
    Six = 6,
    Seven = 7,
    Eight = 8,
    Nine = 9,
    Ten = 10,
    Jack = 11,
    Queen = 12,
    King = 13
}

export enum CardColor 
{
    Red,
    Black
}

export enum CardState
{
    InStock,
    InWaste,
    InFoundation,
    InPile,
    OnCalibrate
}
