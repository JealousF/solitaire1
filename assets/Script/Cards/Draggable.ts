import Card from "./Card";
import GameManager from "../GameManager";
import { CardState } from "./CardTypes";
import Selected from "../CardPlaces/Selected";
import Foundation from "../CardPlaces/Foundation";
import Waste from "../CardPlaces/Waste";
import Pile from "../CardPlaces/Pile";


const {ccclass, property} = cc._decorator;

@ccclass
export default class Draggable extends cc.Component 
{
    public OffsetToMouse : cc.Vec2

    private BeginTouchPos : cc.Vec2

    OrderInLayer : number

    // onLoad () {}

    onEnable()
    {
        this.node.on(cc.Node.EventType.TOUCH_START,this.OnMouseDown,this)
        this.node.on(cc.Node.EventType.TOUCH_MOVE,this.OnMouseDrag,this)
        this.node.on(cc.Node.EventType.TOUCH_END,this.OnMouseUp,this)
        this.node.on(cc.Node.EventType.TOUCH_CANCEL,this.OnMouseCancel,this)
    }

    onDisable()
    {
        this.node.off(cc.Node.EventType.TOUCH_START,this.OnMouseDown,this)
        this.node.off(cc.Node.EventType.TOUCH_MOVE,this.OnMouseDrag,this)
        this.node.off(cc.Node.EventType.TOUCH_END,this.OnMouseUp,this)
        this.node.off(cc.Node.EventType.TOUCH_CANCEL,this.OnMouseCancel,this)
    }

    // update (dt) {}

    OnMouseDown(event : cc.Event.EventTouch)
    {
        this.BeginTouchPos = this.node.position
        if(this.getComponent(Card).State == CardState.InPile)
        {
            var pile = GameManager.instance.GetPileOfCard(this.node)
            console.log(pile.node.name)
            var index = pile.cards.indexOf(this.getComponent(Card))
            console.log(index)
            for(let i = index ;i < pile.cards.length; i++)
            {
                Selected.stack.push(pile.cards[i])
            }
        }
        else
        {
            Selected.stack.push(this.getComponent(Card))
        }
        if(this.node.parent.getComponent(cc.Layout) != null)
        {
             this.node.parent.getComponent(cc.Layout).enabled = false
        }
        this.OrderInLayer = this.node.parent.getLocalZOrder()
        this.node.parent.setLocalZOrder(53)
        for(let i=0; i < Selected.stack.length;i++)
        {
            Selected.stack[i].getComponent(Draggable).OffsetToMouse = Selected.stack[i].node.position.sub(GameManager.instance.node.convertTouchToNodeSpace(event.touch))
        }
    }

    OnMouseDrag(event : cc.Event.EventTouch)
    {
        Selected.stack.forEach
        (card =>
             {
                 card.node.position = card.getComponent(Draggable).OffsetToMouse.add(GameManager.instance.node.convertTouchToNodeSpace(event.touch))
             }
        )
    }

    OnMouseUp(event : cc.Event.EventTouch)
    {
        this.node.parent.setLocalZOrder(this.OrderInLayer)

        let CurrentParentLayout = this.node.parent.getComponent(cc.Layout)

        let PlaceThrowed

        if(cc.pDistance(this.node.position,this.BeginTouchPos)< 0.15)
        {
            console.log("AutoAdd")
            PlaceThrowed = GameManager.instance.AutoFindCardHolderToAdd(this.getComponent(Card))
        }
        else
        {
            PlaceThrowed = GameManager.instance.CheckBoudingBoxDrop(this.node)
        }
        
        if(PlaceThrowed !=null)
        {
            if(PlaceThrowed  instanceof Foundation && PlaceThrowed.canAddCard(this.getComponent(Card)) && Selected.stack.length ==1)
            {
                GameManager.instance.AddToActionList(Selected.stack)
                console.log("Throw in Foundation")
                switch (this.getComponent(Card).State)
                {
                    case CardState.InWaste:
                        {
                            console.log(this.node.parent.name)
                            let waste = this.node.parent.getComponent(Waste)
                            if(waste !=null)
                            {
                                waste.RemoveTopCard()
                            }
                            else
                            {
                                console.log(null)
                            }
                            break
                        }
                        case CardState.InFoundation :
                        {
                            console.log(this.node.parent.name)
                            let found = this.node.parent.getComponent(Foundation)
                            if(found != null)
                            {
                                found.RemoveTopCard()
                            }
                            else
                            {
                                console.log(null)
                            }
                            break
                        }
                        case CardState.InPile:
                        {
                            console.log(this.node.parent.name)
                            let pile = this.node.parent.getComponent(Pile)
                            if(pile != null)
                            {
                                pile.RemoveTopCard()
                            }
                            else
                            {
                                console.log(null)
                            }
                            break
                        }
                }
                PlaceThrowed.AddCard(this.getComponent(Card))

            }
            else if(PlaceThrowed instanceof Pile && PlaceThrowed.canAddCard(this.getComponent(Card)))
            {
                GameManager.instance.AddToActionList(Selected.stack)
                console.log(PlaceThrowed.canAddCard(this.getComponent(Card)))
                console.log("Throw in Pile")
                if(Selected.stack.length ==1)
                {
                    switch (this.getComponent(Card).State)
                    {
                        case CardState.InWaste:
                        {
                            console.log(this.node.parent.name)
                            let waste = this.node.parent.getComponent(Waste)
                            if(waste !=null)
                            {
                                waste.RemoveTopCard()
                            }
                            else
                            {
                                console.log(null)
                            }
                            break
                        }
                        case CardState.InFoundation :
                        {
                            console.log(this.node.parent.name)
                            let found = this.node.parent.getComponent(Foundation)
                            if(found != null)
                            {
                                found.RemoveTopCard()
                            }
                            else
                            {
                                console.log(null)
                            }
                            break

                        }
                        case CardState.InPile:
                        {
                            console.log(this.node.parent.name)
                            let pile = this.node.parent.getComponent(Pile)
                            if(pile != null)
                            {
                                pile.RemoveTopCard()
                            }
                            else
                            {
                                console.log(null)
                            }
                            break
                        }
                    }
                    PlaceThrowed.AddCardObject(this.getComponent(Card))

                }
                else if(Selected.stack.length >1)
                {
                    for(let i=0; i< Selected.stack.length; i++)
                    {
                        console.log(this.node.parent.name)
                        this.node.parent.getComponent(Pile).RemoveTopCard()
                    }
                    PlaceThrowed.AddStackCards(Selected.stack)
                }
            }
        }
        Selected.Clear()
        if(CurrentParentLayout != null)
        {
             CurrentParentLayout.enabled = true
        }
        else
        {
            this.node.position = cc.Vec2.ZERO
        }
    }

    OnMouseCancel(event : cc.Event.EventTouch)
    {
        if(this.node.parent.getComponent(cc.Layout))
        {       
         this.node.parent.getComponent(cc.Layout).enabled = true
        }
        else
        {
            this.node.position = cc.Vec2.ZERO
        }
        this.node.parent.setLocalZOrder(this.OrderInLayer)
        Selected.Clear()
   }
}
