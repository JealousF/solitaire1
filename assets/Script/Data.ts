import { CardValue, CardSuit } from "./Cards/CardTypes";
import Card from "./Cards/Card";
import Draggable from "./Cards/Draggable";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Data
 {

    ParentNode : string
    Value : CardValue
    Suit : CardSuit
    Draggable : boolean
    IndexInParent : number

    public Create(card : Card)
    {
        this.ParentNode = card.node.parent.name
        this.Value = card.Value
        this.Suit = card.Suit
        this.Draggable = card.getComponent(Draggable).enabled
        this.IndexInParent = card.node.parent.children.indexOf(card.node,0)
    }
}
