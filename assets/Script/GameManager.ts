import Stock from "./CardPlaces/Stock";
import Waste from "./CardPlaces/Waste";
import Foundation from "./CardPlaces/Foundation";
import Pile from "./CardPlaces/Pile";
import Card from "./Cards/Card";
import { CardSuit, CardValue, CardState } from "./Cards/CardTypes";
import Setting from "./Setting/Setting";
import Draggable from "./Cards/Draggable";
import CardFlipper from "./Cards/CardFlipper";
import PreWaste from "./CardPlaces/PreWaste";
import Action from "./Action";
import Data from "./Data";
import CardHolder from "./CardPlaces/CardHolder";
import Selected from "./CardPlaces/Selected";

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameManager extends cc.Component
 {
    @property(cc.Node)
    StockNode : cc.Node = null

    @property(cc.Node)
    PrenWasteNode : cc.Node = null

    @property(cc.Node)
    WasteNode : cc.Node = null

    @property(cc.Node)
    Foundation1 : cc.Node = null

    @property(cc.Node)
    Foundation2 : cc.Node = null

    @property(cc.Node)
    Foundation3 : cc.Node = null

    @property(cc.Node)
    Foundation4: cc.Node = null

    @property(cc.Node)
    PlayDeckArea1 : cc.Node = null

    @property(cc.Node)
    PlayDeckArea2: cc.Node = null

    @property(cc.Node)
    PlayDeckArea3: cc.Node = null

    @property(cc.Node)
    PlayDeckArea4 : cc.Node = null

    @property(cc.Node)
    PlayDeckArea5: cc.Node = null

    @property(cc.Node)
    PlayDeckArea6: cc.Node = null

    @property(cc.Node)
    PlayDeckArea7: cc.Node = null

    @property(cc.Node)
    canvas : cc.Node = null

    @property(cc.Prefab)
    CardPrefab : cc.Prefab = null

    @property(cc.Node)
    ParticalWin : cc.Node = null

    @property(cc.Node)
    HintButtonNode : cc.Node = null

    @property(cc.Node)
    UndoButtonNode : cc.Node = null


    public  stock : Stock = null

    public  waste : Waste = null

    public Foundations : Foundation[] = []

    public Piles : Pile[] = []

    public static instance : GameManager=null

    public ListAction : Action[] = []

    public ListNumberToAction : number[] = []

    public fullDeck : Card[] = []


    onLoad()
    {
        GameManager.instance = this
    }

    start ()
    {
        this.waste = this.WasteNode.getComponent(Waste)

        this.Piles.push(this.PlayDeckArea1.getComponent(Pile))
         this.Piles.push(this.PlayDeckArea2.getComponent(Pile))
         this.Piles.push(this.PlayDeckArea3.getComponent(Pile))
         this.Piles.push(this.PlayDeckArea4.getComponent(Pile))
         this.Piles.push(this.PlayDeckArea5.getComponent(Pile))
         this.Piles.push(this.PlayDeckArea6.getComponent(Pile))
         this.Piles.push(this.PlayDeckArea7.getComponent(Pile))

         this.Foundations.push(this.Foundation1.getComponent(Foundation))
         this.Foundations.push(this.Foundation2.getComponent(Foundation))
         this.Foundations.push(this.Foundation3.getComponent(Foundation))
         this.Foundations.push(this.Foundation4.getComponent(Foundation))

         this.stock = this.StockNode.getComponent(Stock)
        this.GenerateCards()
        this.ShuffleCards(this.stock.cards)
        this.DealingCards()
    }

    GenerateCards() : void
    {

        for (let j = 1; j <= 4; j++)
        {
            for (let k = 1; k <= 13; k++)
            {
                var card = cc.instantiate(this.CardPrefab).getComponent(Card)
                card.node.parent = this.canvas
                card.Value =k;
                card.Suit=j
                this.stock.AddCardObject(card)
                this.fullDeck.push(card)
            }
        }
    }

    ShuffleCards(cards : Card[])
    {
        /**
         * Shuffle the cards
         */
        let shuffleIndex = cards.length, randomIndex, tempCard;
        while (shuffleIndex !== 0) {
            randomIndex = Math.floor(Math.random() * shuffleIndex--);
            tempCard = cards[randomIndex];
            cards[randomIndex] = cards[shuffleIndex];
            cards[shuffleIndex] = tempCard;
        }

        return cards;
    }

    DealingCards()
    {
        for(let i=0;i< this.Piles.length;i++)
        {
            for(let j=0; j<i+1;j++)
            {
                this.Piles[i].AddCardObject(this.stock.Draw())
            }
        }

        for(let i= 0; i< this.Piles.length ; i++)
        {
            this.Piles[i].TopCard.getComponent(CardFlipper).Flip()
            this.Piles[i].TopCard.getComponent(Draggable).enabled = true
        }

    }

    public DrawFromStock()
    {
        if(this.stock.hasCard())
        {
            let cards  = []
            while(cards.length < Setting.NumberToDraw)
            {
                if(!this.stock.hasCard()) break;
                cards.unshift(this.stock.Draw())
            }
            this.AddToActionList(cards)
            for(let i=cards.length-1 ;i >= 0;i--)
            {
                this.waste.AddCard(cards[i])
            }
        }
        else
        {
            console.log("Stock out of Card")
            let cards = this.waste.GiveBackToStock()
            let savecards = cards.reverse()
            this.AddToActionList(savecards)
            for(let i = 0; i < cards.length ;i++)
            {
                this.stock.AddCardObject(cards[i])
            }
            this.waste.EnableUpdate()
        }
    }

    public GetPileOfCard(node : cc.Node) : Pile
    {
        for(let i=0; i< this.Piles.length; i++)
            {
                if(node.parent == this.Piles[i].node)
                {
                    return this.Piles[i]
                }
            }
    }

    public CheckBoudingBoxDrop(Xnode : cc.Node) : Object
    {
        for(let i=0 ; i< this.Foundations.length ; i++)
        {
            if(this.Foundations[i].node == Xnode.parent) continue
            let theRect = this.Foundations[i].node.getBoundingBoxToWorld()
            let theRect2 = Xnode.getBoundingBoxToWorld()
            if(theRect.intersects(theRect2))
            {
                console.log(this.Foundations[i].name)
                return this.Foundations[i]
            }
        }

        for(let i=0; i< this.Piles.length ;i++)
        {
            if(this.Piles[i].node == Xnode.parent) continue
            let theRect
            if(this.Piles[i].cards.length <=1)
            {
                theRect = this.Piles[i].node.getBoundingBoxToWorld()
            }
            else
            {
                theRect = this.Piles[i].TopCard.node.getBoundingBoxToWorld()
            }
            let theRect2 = Xnode.getBoundingBoxToWorld()
            if(theRect.intersects(theRect2))
            {
                console.log(this.Piles[i].name)
                return this.Piles[i]
            }
        }

        return null
    }


    FindCardByValueAndSuit(suit : CardSuit, value : CardValue) : Card
    {
        for(let i=0; i< this.fullDeck.length; i++)
        {
            if(this.fullDeck[i].Value == value && this.fullDeck[i].Suit == suit)
            {
                return this.fullDeck[i]
            }
        }
    }

    FindParentTypeByName(name : string) : cc.Component
    {
        switch(name)
        {
            case this.StockNode.name :
            {
                return this.StockNode.getComponent(Stock)
            }
            case this.PrenWasteNode.name :
            {
                return this.PrenWasteNode.getComponent(PreWaste)
            }
            case this.WasteNode.name :
            {
                return this.WasteNode.getComponent(Waste)
            }
            case this.Foundation1.name :
            {
                return this.Foundation1.getComponent(Foundation)
            }
            case this.Foundation2.name :
            {
                return this.Foundation2.getComponent(Foundation)
            }
            case this.Foundation3.name :
            {
                return this.Foundation3.getComponent(Foundation)
            }
            case this.Foundation4.name :
            {
                return this.Foundation4.getComponent(Foundation)
            }
            case this.PlayDeckArea1.name :
            {
                return this.PlayDeckArea1.getComponent(Pile)
            }
            case this.PlayDeckArea2.name :
            {
                return this.PlayDeckArea2.getComponent(Pile)
            }
            case this.PlayDeckArea3.name :
            {
                return this.PlayDeckArea3.getComponent(Pile)
            }
            case this.PlayDeckArea4.name :
            {
                return this.PlayDeckArea4.getComponent(Pile)
            }
            case this.PlayDeckArea5.name :
            {
                return this.PlayDeckArea5.getComponent(Pile)
            }
            case this.PlayDeckArea6.name :
            {
                return this.PlayDeckArea6.getComponent(Pile)
            }
            case this.PlayDeckArea7.name :
            {
                return this.PlayDeckArea7.getComponent(Pile)
            }

            default : null
        }
    }


    public AddToActionList(cards : Card[])
    {
        let number = cards.length;
        this.ListNumberToAction.push(number)
        for(let i=0; i<cards.length; i++)
        {
            let action = new Action()
            action.SetUp(cards[i])
            this.ListAction.push(action)
        }
    }


    public UndoAction()
    {
        if(this.ListNumberToAction.length >0)
        {
            let number = this.ListNumberToAction.pop()
            console.log(number)
            let action = []
            for(let i=0 ;i<number ;i++)
            {
                action.unshift(this.ListAction.pop())
            }
    
            while(action.length > 0)
            {
                let at = action.shift()
                let card = this.FindCardByValueAndSuit(at.Suit,at.Value)
                let oldParent = this.FindParentTypeByName(at.ParentName)
                if(at.isCardBack)
                {
                    card.ToggleCardFace(false)
                }
                if(card.node.parent == oldParent.node) continue
                if(card.node.parent.getComponent(CardHolder) != null)
                {
                    console.log(card.node.parent.name)
                    card.node.parent.getComponent(CardHolder).RemoveCard()
                }
                console.log(at.ParentName)
                console.log(oldParent)
                if(oldParent instanceof Stock)
                {
                    oldParent.AddCardObject(card,true)
                }
                else if(oldParent instanceof Waste)
                {
                    oldParent.AddCard(card)
                }
                else if(oldParent instanceof Pile)
                {
                    oldParent.AddCardObject(card)
                }
                else if(oldParent instanceof Foundation)
                {
                    oldParent.AddCard(card)
                }
                else if(oldParent instanceof PreWaste)
                {
                    oldParent.AddCardObject(card)
                }
                if(at.isCardBack)
                {
                    card.ToggleCardFace(false)
                }
            }
        }
    }

    public HintButton()
    {
        console.log("ClickHint")
        if(this.waste.TopCard != null)
        {
            for(let i=0;i<this.Foundations.length;i++)
            {
                if(this.Foundations[i].canAddCard(this.waste.TopCard))
                {
                    this.waste.TopCard.Shake()
                    if(this.Foundations[i].TopCard != null)
                    {
                        this.Foundations[i].TopCard.Shake()
                    }
                    else
                    {
                        this.Foundations[i].Shake()
                    }
                    console.log("MoveCardInWasteToAFoundation")
                    return
                }
            }
            for(let i=0;i<this.Piles.length;i++)
            {
                if(this.Piles[i].canAddCard(this.waste.TopCard))
                {
                    this.waste.TopCard.Shake()
                    if(this.Piles[i].TopCard != null)
                    {
                        this.Piles[i].TopCard.Shake()
                    }
                    else
                    {
                        this.Piles[i].Shake()
                    }
                    console.log("MoveCardInWasteToAPile")
                    return
                }
            }
        }

        for(let i=0;i<this.Piles.length;i++)
        {
            if(this.Piles[i].TopCard !=null)
            {
                console.log(this.Piles[i].TopCard.Value)
                console.log(this.Piles[i].TopCard.Suit)
                for(let j=0;j<this.Foundations.length;j++)
                {
                    if(this.Foundations[j].canAddCard(this.Piles[i].TopCard))
                    {
                        this.Piles[i].TopCard.Shake()
                        if(this.Foundations[j].TopCard != null)
                        {
                            this.Foundations[j].TopCard.Shake()
                        }
                        else
                        {
                            this.Foundations[j].Shake()
                        }
                        return
                    }
                }

                for(let j=0;j<this.Piles.length;j++)
                {
                    if(i==j) continue
                    if(this.Piles[j].canAddCard(this.Piles[i].TopCard))
                    {
                        this.Piles[i].TopCard.Shake()
                        if(this.Piles[j].TopCard != null)
                        {
                            this.Piles[j].TopCard.Shake()
                        }
                        else
                        {
                            this.Piles[j].Shake()
                        }
                        return
                    }
                }
            }
        }
        if(this.PrenWasteNode.getComponent(PreWaste).hasCard() && this.waste.hasCard() || this.stock.hasCard())
        {
            this.stock.Shake()
        }
    }

    public AutoFindCardHolderToAdd(card : Card) : Object
    {
        if(Selected.stack.length == 1)
        {
            for(let i=0; i< this.Foundations.length; i++)
            {
                if(this.Foundations[i].canAddCard(card))
                {
                    return this.Foundations[i]
                }
            }

            for(let i=0; i<this.Piles.length ;i++)
            {
                if(this.Piles[i].canAddCard(card))
                {
                    return this.Piles[i]
                }
            }
        }
        else if(Selected.stack.length > 1)
        {
            for(let i=0; i<this.Piles.length ;i++)
            {
                if(this.Piles[i].canAddCard(card))
                {
                    return this.Piles[i]
                }
            }
        }
        return null
    }

    public CheckGamewin()
    {
        if(this.IsWin())
        {
            this.fullDeck.forEach(card =>card.getComponent(Draggable).enabled = false)
            this.HintButtonNode.active = false
            this.UndoButtonNode.active = false
            this.ParticalWin.active = true
            this.node.destroy()
        }
    }

    private IsWin() : boolean
    {
        for(let i=0;i<this.Foundations.length;i++)
        {
            if(this.Foundations[i].cards.length <13)
            return false
        }
        return true
    }

}
