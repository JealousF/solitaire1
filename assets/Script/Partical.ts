// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Partical extends cc.Component
 {
     @property(cc.Node)
     public CongratulationsText : cc.Label =null
     onEnable()
     {
         let action = cc.scaleTo(0.5,1.3)
         this.schedule(this.ScaleLable,1,3,0)
     }

     ScaleLable()
     {
         let action = cc.scaleTo(0.5,1.3)
         let action1 = cc.scaleTo(0.5,1)
         let fullact = cc.sequence(action,action1)
         this.node.runAction(fullact)
     }
}
