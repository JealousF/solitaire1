
const {ccclass, property} = cc._decorator;

@ccclass
export default class Setting 
 {
     public static NumberToDraw : number = 1
     public static ShuffleTime : number = 30

     public static TimeToFlip : number = 0.2
     public static TimeEffect : number = 0.2
}
