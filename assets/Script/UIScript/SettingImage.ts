// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class SettingImage extends cc.Component
{
    public Show()
    {
        this.node.position = new cc.Vec2(-360,0)
        this.node.active = true;
        let action = cc.moveTo(0.2,new cc.Vec2(360,0))
        this.node.runAction(action)
    }

    public Hide()
    {
        this.node.position = new cc.Vec2(360,0)
        let action = cc.moveTo(0.2,new cc.Vec2(-360,0))
        this.node.runAction(action)
        this.scheduleOnce(()=>this.node.active = false,0.2)
    }
}
