import Setting from "../Setting/Setting";

// Learn TypeScript:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class TopPanel extends cc.Component
{
    @property(cc.Node)
    Setting : cc.Node = null

    @property(cc.Node)
    TimeDisPlay : cc.Node = null

    @property(cc.Label)
    DrawMode : cc.Label = null

    private minute : number = 0
    private second : number =0

    onEnable()
    {
        this.DrawMode.getComponent(cc.Label).string = Setting.NumberToDraw.toString()
        this.EnableUpdateTime()
    }

    public EnableUpdateTime()
    {
        this.schedule(this.TimeUp,1,Infinity,0)
    }

    public DisableUpdateTime()
    {
        this.unschedule(this.TimeUp)
    }

    TimeUp()
    {
        this.second += 1
        if(this.second == 60)
        {
            this.minute +=1
            this.second =0
        }
        if(this.second<10)
        {
            this.TimeDisPlay.getComponent(cc.Label).string = this.minute.toString() + ":" + "0" + this.second.toString()
        }
        else
        {
            this.TimeDisPlay.getComponent(cc.Label).string = this.minute.toString() + ":" + this.second.toString()
        }
    }

    onDisable()
    {
        this.DisableUpdateTime()
    }
}
